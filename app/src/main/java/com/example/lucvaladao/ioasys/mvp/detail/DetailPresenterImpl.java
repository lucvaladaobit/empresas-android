package com.example.lucvaladao.ioasys.mvp.detail;

import com.example.lucvaladao.ioasys.entity.Company;
import com.example.lucvaladao.ioasys.mvp.ApiService;
import com.example.lucvaladao.ioasys.mvp.detail.DetailInteractor.GetCompanyListener;

import javax.inject.Inject;

/**
 * Created by lucvaladao on 4/12/18.
 */

class DetailPresenterImpl implements DetailPresenter, GetCompanyListener {

    @Inject
    DetailView detailView;
    @Inject
    DetailInteractor detailInteractor;
    @Inject
    ApiService apiService;

    @Inject
    public DetailPresenterImpl(DetailView detailView, DetailInteractor detailInteractor, ApiService apiService) {
        this.detailView = detailView;
        this.detailInteractor = detailInteractor;
        this.apiService = apiService;
    }

    @Override
    public void getCompany(String companyName) {
        detailInteractor.getCompany(companyName, this);
    }

    @Override
    public void getCompanyOnSuccess(Company company) {
        detailView.fillFields(company);
    }

    @Override
    public void getCompanyOnFailure(String message) {
        detailView.showError(message);
    }
}
