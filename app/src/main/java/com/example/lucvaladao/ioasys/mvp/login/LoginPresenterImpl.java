package com.example.lucvaladao.ioasys.mvp.login;

import com.example.lucvaladao.ioasys.entity.UserInfo;
import com.example.lucvaladao.ioasys.mvp.ApiService;
import com.example.lucvaladao.ioasys.mvp.login.LoginInteractor.LoginListener;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lucvaladao on 4/11/18.
 */

class LoginPresenterImpl implements LoginPresenter, LoginListener {

    @Inject
    LoginView loginView;
    @Inject
    ApiService apiService;
    @Inject
    LoginInteractor loginInteractor;

    @Inject
    public LoginPresenterImpl(LoginView loginView, LoginInteractor loginInteractor, ApiService apiService) {
        this.loginView = loginView;
        this.loginInteractor = loginInteractor;
        this.apiService = apiService;
    }

    @Override
    public void doLogin(String email, String password) {
        loginInteractor.doLogin(email, password, this);
    }

    @Override
    public void onSuccess(String message) {
        loginView.showToast(message);
        loginView.goToCompanyActivity();
    }

    @Override
    public void onError(String message) {
        loginView.loginCancelled(message);
    }
}
