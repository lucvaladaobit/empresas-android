package com.example.lucvaladao.ioasys.di.component;

import android.app.Application;

import com.example.lucvaladao.ioasys.application.AndroidSampleApp;
import com.example.lucvaladao.ioasys.di.module.ActivityBuilder;
import com.example.lucvaladao.ioasys.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Created by lucvaladao on 4/11/18.
 */

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, AppModule.class, ActivityBuilder.class})
public interface AppComponent extends AndroidInjector<DaggerApplication> {

    void inject (AndroidSampleApp app);

    @Override
    void inject(DaggerApplication instance);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }
}
