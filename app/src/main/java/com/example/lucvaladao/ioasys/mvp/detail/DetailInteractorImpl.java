package com.example.lucvaladao.ioasys.mvp.detail;

import android.content.SharedPreferences;

import com.example.lucvaladao.ioasys.entity.CompaniesResult;
import com.example.lucvaladao.ioasys.entity.Company;
import com.example.lucvaladao.ioasys.entity.UserInfo;
import com.example.lucvaladao.ioasys.mvp.ApiService;
import com.google.gson.Gson;

import javax.inject.Inject;

import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lucvaladao on 4/12/18.
 */

class DetailInteractorImpl implements DetailInteractor {

    @Inject
    ApiService apiService;
    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    public DetailInteractorImpl(ApiService apiService, SharedPreferences sharedPreferences) {
        this.apiService = apiService;
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void getCompany(String companyName, final GetCompanyListener getCompanyListener) {
        UserInfo userInfo = new Gson().
                fromJson(sharedPreferences.
                        getString("userInfo", null), UserInfo.class);

        apiService.getCompany(userInfo.getUid(),
                userInfo.getAccessToken(),
                userInfo.getClient(), companyName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleSubscriber<CompaniesResult>() {
                    @Override
                    public void onSuccess(CompaniesResult CompaniesResult) {
                        getCompanyListener.getCompanyOnSuccess(CompaniesResult.getCompanies().get(0));
                    }

                    @Override
                    public void onError(Throwable error) {
                        getCompanyListener.getCompanyOnFailure("Algo aconteceu na requisição das empresas! :(");
                    }
                });
    }
}
