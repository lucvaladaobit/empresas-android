package com.example.lucvaladao.ioasys.di.module;

import com.example.lucvaladao.ioasys.mvp.company.CompanyActivity;
import com.example.lucvaladao.ioasys.mvp.company.CompanyActivityModule;
import com.example.lucvaladao.ioasys.mvp.detail.DetailActivity;
import com.example.lucvaladao.ioasys.mvp.detail.DetailActivityModule;
import com.example.lucvaladao.ioasys.mvp.login.LoginActivity;
import com.example.lucvaladao.ioasys.mvp.login.LoginActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by lucvaladao on 4/11/18.
 */

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    abstract LoginActivity bindLoginActivity();

    @ContributesAndroidInjector(modules = CompanyActivityModule.class)
    abstract CompanyActivity bindCompanyActivity();

    @ContributesAndroidInjector(modules = DetailActivityModule.class)
    abstract DetailActivity bindDetailActivity();
}
