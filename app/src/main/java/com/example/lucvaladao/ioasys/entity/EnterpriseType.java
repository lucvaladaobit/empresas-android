package com.example.lucvaladao.ioasys.entity;

import com.google.gson.annotations.SerializedName;

public class EnterpriseType {

    @SerializedName("enterprise_type_name")
    private String type;

    public EnterpriseType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
