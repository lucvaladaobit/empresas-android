package com.example.lucvaladao.ioasys.mvp.detail;

import android.content.SharedPreferences;

import com.example.lucvaladao.ioasys.mvp.ApiService;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by lucvaladao on 4/12/18.
 */

@Module
public abstract class DetailActivityModule {

    @Provides
    static DetailPresenter provideDetailPresenter(DetailView detailView, DetailInteractor detailInteractor, ApiService apiService) {
        return new DetailPresenterImpl(detailView, detailInteractor, apiService);
    }

    @Provides
    static DetailInteractor provideDetailInteractor(ApiService apiService, SharedPreferences sharedPreferences) {
        return new DetailInteractorImpl(apiService, sharedPreferences);
    }

    @Binds
    abstract DetailView provideDetailView(DetailActivity detailActivity);
}
