package com.example.lucvaladao.ioasys.mvp.company;

import android.content.SharedPreferences;

import com.example.lucvaladao.ioasys.entity.CompaniesResult;
import com.example.lucvaladao.ioasys.entity.UserInfo;
import com.example.lucvaladao.ioasys.mvp.ApiService;
import com.google.gson.Gson;

import javax.inject.Inject;

import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lucvaladao on 4/12/18.
 */

class CompanyInteractorImpl implements CompanyInteractor {

    @Inject
    ApiService apiService;
    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    public CompanyInteractorImpl(ApiService apiService, SharedPreferences sharedPreferences) {
        this.apiService = apiService;
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void getCompaniesRemote(final GetCompaniesListener getCompaniesListener) {
        UserInfo userInfo = new Gson().
                fromJson(sharedPreferences.
                        getString("userInfo", null), UserInfo.class);

        apiService.getCompanies(userInfo.getUid(),
                userInfo.getAccessToken(),
                userInfo.getClient())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleSubscriber<CompaniesResult>() {
                    @Override
                    public void onSuccess(CompaniesResult companiesResult) {
                        getCompaniesListener.getCompaniesOnSuccess(companiesResult.getCompanies());
                    }

                    @Override
                    public void onError(Throwable error) {
                        getCompaniesListener.getCompaniesOnFailure("Algo aconteceu na requisição das empresas! :(");
                    }
                });
    }
}
