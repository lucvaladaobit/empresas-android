package com.example.lucvaladao.ioasys.mvp.company;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lucvaladao.ioasys.R;
import com.example.lucvaladao.ioasys.entity.Company;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucvaladao on 4/12/18.
 */

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.CompanyViewHolder> {

    private Context mContext;
    private List<Company> companyList;
    private CompanyAdapterInterface mListener;

    public CompanyAdapter(Context context, List<Company> companyList) throws Exception {
        this.mContext = context;
        this.companyList = companyList;
        if (context instanceof CompanyAdapterInterface) {
            mListener = (CompanyAdapterInterface) context;
        } else {
            throw new Exception("Not implemented!");
        }
    }

    @Override
    public CompanyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_company, parent, false);
        return new CompanyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(CompanyViewHolder holder, int position) {
        final Company company = companyList.get(position);
        holder.companyNameTextView.setText(company.getName());
        holder.companyCountryTextView.setText(company.getCountry());
        holder.companyTypeTextView.setText(company.getEnterpriseType().getType());
        holder.companyCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.goToDetailActivityFromAdapter(company.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return companyList.size();
    }

    class CompanyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.companyNameTextView)
        TextView companyNameTextView;
        @BindView(R.id.companyTypeTextView)
        TextView companyTypeTextView;
        @BindView(R.id.companyCountryTextView)
        TextView companyCountryTextView;
        @BindView(R.id.companyCardView)
        CardView companyCardView;

        public CompanyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


