package com.example.lucvaladao.ioasys.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucvaladao on 4/11/18.
 */

public class Company {

    @SerializedName("enterprise_name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("country")
    private String country;
    @SerializedName("enterprise_type")
    EnterpriseType enterpriseType;

    public Company(String name, String description, String country, EnterpriseType enterpriseType) {
        this.name = name;
        this.description = description;
        this.country = country;
        this.enterpriseType = enterpriseType;
    }

    public EnterpriseType getEnterpriseType() {return enterpriseType;}

    public String getName() {
        return name;
    }

    public String getCountry() {return country;}

    public String getDescription() {
        return description;
    }
}

