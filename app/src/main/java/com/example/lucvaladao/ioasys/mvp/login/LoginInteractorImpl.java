package com.example.lucvaladao.ioasys.mvp.login;

import android.content.SharedPreferences;

import com.example.lucvaladao.ioasys.entity.UserInfo;
import com.example.lucvaladao.ioasys.mvp.ApiService;
import com.google.gson.Gson;

import javax.inject.Inject;

import retrofit2.Response;
import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by lucvaladao on 4/12/18.
 */

class LoginInteractorImpl implements LoginInteractor {

    @Inject
    ApiService apiService;

    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    public LoginInteractorImpl(ApiService apiService, SharedPreferences sharedPreferences) {
        this.apiService = apiService;
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void doLogin(String email, String password, final LoginListener loginListener) {
        apiService.doLogin(email, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleSubscriber<Response<UserInfo>>() {
                    @Override
                    public void onSuccess(Response<UserInfo> userInfoResponse) {
                        if (userInfoResponse.code() == 200) {
                            UserInfo userInfo = new UserInfo(userInfoResponse.headers().get("uid"),
                                    userInfoResponse.headers().get("client"),
                                    userInfoResponse.headers().get("access-token"));
                            loginListener.onSuccess(userInfo.toString());
                            saveUserInfo(userInfo);
                        } else {
                            loginListener.onError("Houve comunicação com o servidor, porém o usuário não foi encontrado! :( \n" + userInfoResponse.code());
                        }
                    }

                    @Override
                    public void onError(Throwable error) {
                        loginListener.onError("Usuário não encontrado! :(");
                    }
                });
    }

    @Override
    public void saveUserInfo(UserInfo userInfo) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("userInfo", new Gson().toJson(userInfo));
        editor.apply();
    }
}
