package com.example.lucvaladao.ioasys.mvp.login;

import com.example.lucvaladao.ioasys.entity.UserInfo;

/**
 * Created by lucvaladao on 4/12/18.
 */

interface LoginInteractor {

    interface LoginListener {
        void onSuccess(String message);
        void onError(String message);
    }

    void doLogin(String email, String password, LoginListener loginListener);
    void saveUserInfo (UserInfo userInfo);
}

interface LoginPresenter {
    void doLogin (String email, String password);
}

interface LoginView {
    void showToast(String message);
    void loginCancelled(String message);
    void showProgress ();
    void hideProgress ();
    void goToCompanyActivity();
    void showLoginButton();
    void hideLoginButton();
}
