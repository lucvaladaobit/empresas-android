package com.example.lucvaladao.ioasys.mvp.login;

import android.content.SharedPreferences;

import com.example.lucvaladao.ioasys.mvp.ApiService;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by lucvaladao on 4/11/18.
 */

@Module
public abstract class LoginActivityModule {

    @Provides
    static LoginPresenter provideLoginPresenter(LoginView loginView, LoginInteractor loginInteractor, ApiService apiService) {
        return new LoginPresenterImpl(loginView, loginInteractor, apiService);
    }

    @Provides
    static LoginInteractor provideLoginInteractor(ApiService apiService, SharedPreferences sharedPreferences) {
        return new LoginInteractorImpl(apiService, sharedPreferences);
    }

    @Binds
    abstract LoginView provideLoginView(LoginActivity loginActivity);

}
