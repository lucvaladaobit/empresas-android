package com.example.lucvaladao.ioasys.mvp.company;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.lucvaladao.ioasys.R;
import com.example.lucvaladao.ioasys.entity.Company;
import com.example.lucvaladao.ioasys.mvp.ApiService;
import com.example.lucvaladao.ioasys.mvp.detail.DetailActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class CompanyActivity extends DaggerAppCompatActivity implements CompanyView, CompanyAdapterInterface, SearchView.OnQueryTextListener {

    @Inject
    CompanyPresenter companyPresenter;
    @Inject
    ApiService apiService;

    @BindView(R.id.initialFrameLayout)
    FrameLayout initialFrameLayout;
    @BindView(R.id.companyRecyclerView)
    RecyclerView mCompanyRecyclerView;
    @BindView(R.id.companyToolbar)
    Toolbar companyToolbar;
    @BindView(R.id.toolbarLogoImageView)
    ImageView toolbarLogoImageView;

    private List<Company> mCompanies;
    private CompanyAdapter mCompanyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        ButterKnife.bind(this);

        setSupportActionBar(companyToolbar);

        companyPresenter.getCompanies();
        mCompanies = new ArrayList<>();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showCompanies(List<Company> companies) {
        try {
            mCompanyAdapter = new CompanyAdapter(this, companies);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mCompanyRecyclerView.setAdapter(mCompanyAdapter);
        mCompanyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void goToCompanyDetailActivity(String companyName) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("companyName", companyName);
        startActivity(intent);
    }

    @Override
    public void notifyAdapterBasedOnSearch(String query) {
        List<Company> companyList = new ArrayList<>();
        for (Company company : mCompanies) {
            if (company.getName().toLowerCase().contains(query.toLowerCase())) {
                companyList.add(company);
            } else {
                companyList.remove(company);
            }
        }
        showCompanies(companyList);
    }

    @Override
    public void cacheCompanies(List<Company> companies) {
        this.mCompanies = companies;
    }

    @Override
    public void goToDetailActivityFromAdapter(String companyName) {
        goToCompanyDetailActivity(companyName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        SearchView mSearchView = (SearchView) menu.findItem(R.id.search).getActionView();
        mSearchView.setOnQueryTextListener(this);

        SearchView.SearchAutoComplete searchAutoComplete = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHintTextColor(Color.WHITE);
        searchAutoComplete.setTextColor(Color.WHITE);

        ImageView searchCloseIcon = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        searchCloseIcon.setImageResource(R.drawable.ic_clear_search_24dp);

        MenuItemCompat.setOnActionExpandListener(menu.findItem(R.id.search), new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                initialFrameLayout.setVisibility(View.GONE);
                showCompanies(mCompanies);
                toolbarLogoImageView.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                toolbarLogoImageView.setVisibility(View.VISIBLE);
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        notifyAdapterBasedOnSearch(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        notifyAdapterBasedOnSearch(newText);
        return false;
    }
}
