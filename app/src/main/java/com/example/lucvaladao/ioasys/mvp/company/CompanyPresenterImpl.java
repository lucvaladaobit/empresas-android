package com.example.lucvaladao.ioasys.mvp.company;

import com.example.lucvaladao.ioasys.entity.Company;
import com.example.lucvaladao.ioasys.mvp.ApiService;

import java.util.List;

import javax.inject.Inject;

import static com.example.lucvaladao.ioasys.mvp.company.CompanyInteractor.*;

/**
 * Created by lucvaladao on 4/12/18.
 */

class CompanyPresenterImpl implements CompanyPresenter, GetCompaniesListener {

    @Inject
    CompanyView companyView;
    @Inject
    CompanyInteractor companyInteractor;
    @Inject
    ApiService apiService;

    @Inject
    public CompanyPresenterImpl(CompanyView companyView, CompanyInteractor companyInteractor, ApiService apiService) {
        this.companyView = companyView;
        this.companyInteractor = companyInteractor;
        this.apiService = apiService;
    }

    @Override
    public void getCompanies() {
        companyInteractor.getCompaniesRemote(this);
    }

    @Override
    public void getCompaniesOnSuccess(List<Company> companies) {
        companyView.cacheCompanies(companies);
    }

    @Override
    public void getCompaniesOnFailure(String message) {
        companyView.showToast(message);
    }
}
