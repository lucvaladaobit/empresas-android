package com.example.lucvaladao.ioasys.mvp.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.lucvaladao.ioasys.R;
import com.example.lucvaladao.ioasys.entity.UserInfo;
import com.example.lucvaladao.ioasys.mvp.ApiService;
import com.example.lucvaladao.ioasys.mvp.company.CompanyActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import rx.Observer;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity extends DaggerAppCompatActivity implements LoginView {

    @Inject
    LoginPresenter loginPresenter;
    @Inject
    ApiService apiService;

    @BindView(R.id.loginTextInputEditText)
    TextInputEditText loginField;
    @BindView(R.id.passwordTextInputEditText)
    TextInputEditText passwordField;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.loginProgressBar)
    ProgressBar loginProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loginField.setText("testeapple@ioasys.com.br");
        passwordField.setText("12341234");
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginPresenter.doLogin(
                        loginField.getText().toString(),
                        passwordField.getText().toString()
                );
                hideLoginButton();
                showProgress();
            }
        });
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginCancelled(String message) {
        hideProgress();
        showLoginButton();
        showToast(message);
    }

    @Override
    public void showProgress() {
        loginProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loginProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void goToCompanyActivity() {
        hideProgress();
        showLoginButton();
        startActivity(new Intent(this, CompanyActivity.class));
        finish();
    }

    @Override
    public void showLoginButton() {
        loginButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginButton() {
        loginButton.setVisibility(View.GONE);
    }
}
