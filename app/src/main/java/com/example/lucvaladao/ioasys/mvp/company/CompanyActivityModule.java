package com.example.lucvaladao.ioasys.mvp.company;

import android.content.SharedPreferences;

import com.example.lucvaladao.ioasys.mvp.ApiService;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by lucvaladao on 4/12/18.
 */

@Module
public abstract class CompanyActivityModule {

    @Provides
    static CompanyPresenter provideCompanyPresenter(CompanyView companyView, CompanyInteractor companyInteractor, ApiService apiService) {
        return new CompanyPresenterImpl(companyView, companyInteractor, apiService);
    }

    @Provides
    static CompanyInteractor provideCompanyInteractor(ApiService apiService, SharedPreferences sharedPreferences) {
        return new CompanyInteractorImpl(apiService, sharedPreferences);
    }

    @Binds
    abstract CompanyView provideCompanyView(CompanyActivity companyActivity);

}
