package com.example.lucvaladao.ioasys.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.lucvaladao.ioasys.mvp.ApiService;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by lucvaladao on 4/11/18.
 */

@Module
public abstract class AppModule {

    @Binds
    abstract Context provideContext(Application application);

    @Singleton
    @Provides
    static SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences("sharedPref", MODE_PRIVATE);
    }

    @Singleton
    @Provides
    static ApiService getApiService(Retrofit retrofit){
        return retrofit.create(ApiService.class);
    }

    @Singleton
    @Provides
    static GsonConverterFactory provideGsonConverterFactory(){
        return GsonConverterFactory.create();
    }

    @Singleton
    @Provides
    static HttpLoggingInterceptor provideHttpLoggingInterceptor(){
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Singleton
    @Provides
    static OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor){
        return new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();
    }

    @Singleton
    @Provides
    static RxJavaCallAdapterFactory provideRxJavaCallAdapterFactory(){
        return RxJavaCallAdapterFactory.create();
    }

    @Singleton
    @Provides
    static Retrofit provideRetrofit(OkHttpClient okHttpClient,
                                    GsonConverterFactory gsonConverterFactory,
                                    RxJavaCallAdapterFactory rxJavaCallAdapterFactory){
        String mBaseUrl = "http://54.94.179.135:8090/";
        return new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .client(okHttpClient)
                .build();
    }


}
