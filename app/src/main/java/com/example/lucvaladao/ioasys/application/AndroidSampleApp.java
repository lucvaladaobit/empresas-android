package com.example.lucvaladao.ioasys.application;

import com.example.lucvaladao.ioasys.di.component.AppComponent;
import com.example.lucvaladao.ioasys.di.component.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

/**
 * Created by lucvaladao on 4/11/18.
 */

public class AndroidSampleApp extends DaggerApplication {

    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }

}
