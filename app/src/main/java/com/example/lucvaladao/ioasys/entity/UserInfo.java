package com.example.lucvaladao.ioasys.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucvaladao on 4/11/18.
 */

public class UserInfo {

    @SerializedName("uid")
    private String uid;
    @SerializedName("client")
    private String client;
    @SerializedName("access-token")
    private String accessToken;

    public UserInfo(String uid, String client, String accessToken) {
        this.uid = uid;
        this.client = client;
        this.accessToken = accessToken;
    }

    public String getUid() {
        return uid;
    }

    public String getClient() {
        return client;
    }

    public String getAccessToken() {
        return accessToken;
    }

    @Override
    public String toString() {
        return "Uid: " + uid + "\n"
                + "Client: " + client + "\n"
                + "Access Token: " + accessToken;
    }
}
