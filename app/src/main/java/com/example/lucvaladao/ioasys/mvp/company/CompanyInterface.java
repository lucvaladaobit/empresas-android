package com.example.lucvaladao.ioasys.mvp.company;

import com.example.lucvaladao.ioasys.entity.Company;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucvaladao on 4/12/18.
 */

interface CompanyInteractor {

    void getCompaniesRemote(GetCompaniesListener getCompaniesListener);

    interface GetCompaniesListener {
        void getCompaniesOnSuccess(List<Company> companies);
        void getCompaniesOnFailure(String message);
    }
}

interface CompanyPresenter {
    void getCompanies();
}

interface CompanyView {
    void showToast(String message);
    void showCompanies(List<Company> companies);
    void goToCompanyDetailActivity(String companyName);
    void notifyAdapterBasedOnSearch(String query);
    void cacheCompanies(List<Company> companies);
}


interface CompanyAdapterInterface{
    void goToDetailActivityFromAdapter(String companyName);
}