package com.example.lucvaladao.ioasys.mvp.detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lucvaladao.ioasys.R;
import com.example.lucvaladao.ioasys.entity.Company;
import com.example.lucvaladao.ioasys.mvp.ApiService;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class DetailActivity extends DaggerAppCompatActivity implements DetailView {

    @Inject
    DetailPresenter detailPresenter;
    @Inject
    ApiService apiService;
    @BindView(R.id.detailToolbar)
    Toolbar detailToolbar;
    @BindView(R.id.companyNameTextView)
    TextView companyNameTextView;
    @BindView(R.id.descriptionTextView)
    TextView descriptionTextView;
    @BindView(R.id.detailProgressBar)
    ProgressBar detailProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setSupportActionBar(detailToolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();
        String companyDetail = getIntent().getStringExtra("companyName");
        if (companyDetail != null){
            showProgress();
            detailPresenter.getCompany(companyDetail);
        }
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        descriptionTextView.setVisibility(View.GONE);
        detailProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        descriptionTextView.setVisibility(View.VISIBLE);
        detailProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void fillFields(Company company) {
        hideProgress();
        companyNameTextView.setText(company.getName());
        descriptionTextView.setText(company.getDescription());
    }

    @Override
    public void showError(String message) {
        showToast(message);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        } else {
            super.onOptionsItemSelected(item);
            return false;
        }
    }
}
