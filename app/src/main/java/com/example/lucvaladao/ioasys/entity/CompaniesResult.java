package com.example.lucvaladao.ioasys.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lucvaladao on 4/12/18.
 */

public class CompaniesResult {

    @SerializedName("enterprises")
    private List<Company> companies;

    public List<Company> getCompanies() {
        return companies;
    }
}
