package com.example.lucvaladao.ioasys.mvp;


import com.example.lucvaladao.ioasys.entity.CompaniesResult;
import com.example.lucvaladao.ioasys.entity.Company;
import com.example.lucvaladao.ioasys.entity.UserInfo;

import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Single;

/**
 * Created by lucvaladao on 4/11/18.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("api/v1/users/auth/sign_in")
    Single<Response<UserInfo>> doLogin(@Field("email") String email, @Field("password") String password);

    @GET("/api/v1/enterprises")
    Single<CompaniesResult> getCompanies(@Header("uid") String uid, @Header("access-token") String accessToken, @Header("client") String client);

    @GET("/api/v1/enterprises")
    Single<CompaniesResult> getCompany(@Header("uid") String uid, @Header("access-token") String accessToken, @Header("client") String client, @Query("name") String companyName);
}
