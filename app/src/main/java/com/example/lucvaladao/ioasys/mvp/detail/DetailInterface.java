package com.example.lucvaladao.ioasys.mvp.detail;

import com.example.lucvaladao.ioasys.entity.Company;

/**
 * Created by lucvaladao on 4/12/18.
 */

interface DetailInteractor {

    void getCompany(String companyName, GetCompanyListener getCompanyListener);

    interface GetCompanyListener {
        void getCompanyOnSuccess(Company company);
        void getCompanyOnFailure(String message);
    }

}

interface DetailPresenter {
    void getCompany(String companyName);
}

interface DetailView {
    void showToast(String message);
    void showProgress();
    void hideProgress();
    void fillFields(Company company);
    void showError(String message);
}